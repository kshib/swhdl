import os

import requests
from multiprocessing.pool import ThreadPool
import sys

url_search = "https://wallhaven.cc/api/v1/search"
url_wallpapers = []
num_process = 8
args = sys.argv[1:]
api_query = args[0]
if len(args) == 1:
    dir_download = os.getcwd() + "/Downloads/"
else:
    dir_download = args[1]
if not os.path.exists(dir_download):
    os.mkdir(dir_download)

print("Downloading to", dir_download)


def fetch_url(entry):
    path = entry.split("/")[-1]
    if not os.path.exists(path):
        r = requests.get(entry, stream=True)
        if r.status_code == 200:
            with open(dir_download + path, 'wb') as f:
                for chunk in r:
                    f.write(chunk)
    else:
        return "Ignored " + path
    return "Downloaded " + path


response = requests.get(url_search + "?" + api_query).json()
meta = response["meta"]
pages = int(meta["last_page"])
total = meta["total"]
for page in range(1, pages+1):
    response = requests.get(url_search + "?" + api_query + "&page="+str(page)).json()
    for d in response["data"]:
        url_wallpapers.append(d["path"])

wallpapers = ThreadPool(num_process).imap_unordered(fetch_url, url_wallpapers)

for wallpaper in wallpapers:
    print(wallpaper.split("/")[-1])

print("Downloaded to", dir_download)
